# Titanic Machine Learning

This project is an exploration into machine learning using Python and scikit-learn, developed by Samuel J. M. Gilburt. 
The data set and analysis is based on Kaggle's *Titanic: Machine Learning From Disaster* challenge, the full details of 
which are available [here](https://www.kaggle.com/c/titanic).

## Summary

The challenge is a binary classification problem relating to the Titanic disaster - determining whether a passenger 
survived or not.

The project primarily consists of two IPython Notebooks, developed using [Jupyter](https://jupyter.org/) - one which 
attempts to train based on a simple data set with little preprocessing, and a second which attempts to improve results 
by deriving additional dimension from the original data set.

Both notebooks prepare the data for learning (from a training set) using [pandas](https://pandas.pydata.org/) and then 
attempt analysis via two machine learning methods - random forest classifiers and support vector machines - using 
[scikit-learn](http://scikit-learn.org/stable/). Each then produces a number of metrics for classification success, 
including accuracy, f-measure and ROC (displayed using [MatPlotLib](https://matplotlib.org/)).

A full explanation and analysis of results is given below.

## Background

### Problem Area

On 15th April, 1912, the RMS Titanic infamously sank during her maiden voyage from Southampton to New York. According 
to [Geller](https://books.google.co.uk/books?id=dfkiBcUMXw8C), the ship carried enough lifeboats for 1,178 people, but 
was carrying ~2,224 passengers and crew at the time, and only ~722 survived - just ~32.5%. There have been numerous 
investigations into the disaster, but with recent developments in machine learning, new analyses of the incident can be 
performed. Whilst one might argue the use of such investigation after more than 100 years, machine learning 
nevertheless provides the opportunity for new, interesting insight.

This project is an investigation of classification of RMS Titanic passengers, attempting to predict whether they 
survived given a set of features to describe each passenger. It is a binary classification problem, for which there are 
many classification methods available. This project uses two such methods: a Random Forest Classifier and a Support 
Vector Machine, with a focus on how preprocessing can significantly improve classifier results. The data set provided 
by [Kaggle](https://www.kaggle.com/c/titanic) used for the project is described [below](#data-set). The project uses 
the [scikit-learn](http://scikit-learn.org/stable/) package for machine learning and analysis in Python.

### Data Set

The training data set used for the project consists of 891 records, each with 11 features (9 for predictions):
* "PassengerId"
* "Survived"
* "Pclass"
* "Name"
* "Sex"
* "Age"
* "SibSp"
* "Parch"
* "Ticket"
* "Fare"
* "Cabin"
* "Embarked"

The data set was predominantly complete, with only the "Age" and "Cabin" features with a significant number of null 
values. The former has 177 null values while the latter has 687. "Embarked" also had two null values.

Of the 891 records, 342 are listed to have survived, a proportion of ~38.4% – some 6.2% higher than the actual 
proportion of Titanic survivors. However, according to [Geller](https://books.google.co.uk/books?id=dfkiBcUMXw8C), when 
crew and staff are excluded the actual proportion is closer to 37%, so the data can be taken as roughly representative. 
Similarly, the actual proportion of male passengers was around 59.7%, whilst the data set has a proportion of ~64.8%. 
This difference may be attributed to the proportion of male/female children aboard (whose actual records are difficult 
to discern).

## Methodology

### Data Preparation

The original data set is used to produce two training sets – one with minimal preprocessing ("simple set") and another 
with more extensive techniques used ("preprocessed set") in order to attempt to improve results.

For both sets, any null values of are replaced with approximations. For "Age", the median average ages for each gender 
and class are computed (women are on average younger than men, and average age decreases from first down to third 
class) and the null values replaced according to the appropriate gender/class average for the record in question. Other 
features' null values are replaced similarly. All features are converted to numerical values, non-numerical values 
removed and features scaled. 

For the preprocessed set, further features are derived. "Cabin" is split into "CabinLetter" and "CabinNo". "FareBin" 
is derived from "Fare" by binning fares into five categories with intervals of 10.0 up to 50.0+. "NameCount" and 
"Title" are derived from "Name", with the latter including some approximations so each record had a "Mr", "Mrs", 
"Master", "Miss", "Sir" or "Lady" value. "FamilySize" is derived from "SibSp" and "Parch". A few other features are 
combined by multiplication.

Running Principal Component Analysis found that 13 features accounted for 95% of the variance. However, PCA reduces 
accuracy for both classifiers, most likely due to the already relatively low feature count, so PCA is not used for the 
final data set.

*N.B. As a result of analysing the random forest feature importance, five further classes are actually dropped from 
the preprocessed set. See [Results](#results).*

### Random Forest Classifier

Random forests combine numerous randomised decision tree predictors, created from values of a vector sampled 
independently. Large decision trees individually tend to be relatively inaccurate due to high variance or overfitting. 
Random forests reduce the variance by averaging multiple decision trees. Each node in each tree is split using a random 
subset of features. As the number of trees in the forest increases, the generalisation error converges. Combining 
the results of each tree through "bootstrap aggregating", or bagging, allows for relatively low error rates in 
prediction and robustness when variables may be noisy.

For this project, hyperparameter optimisation is performed for the random forest used with the preprocessed set in 
order to find the best parameters for use with the classifier implementation. A simple grid search to investigate a few 
variations on tree feature number, depth and sample split number were run to determine "optimum" parameters. In 
actuality, the search could have been far more extensive, but due to limitations in computation power and time this 
optimisation was not carried out. Despite this, the parameter suggestions (max. features = 4, max. depth = 10, min. 
sample split = 2) still improve the accuracy of the classifier markedly and so are used.

### Support Vector Machine

Support vector machines (SVMs) attempt to separate variables into classifications, with a clear "gap" between data 
points that is as wide as possible. If satisfactory linear separation is not possible, SVMs are able to utilise the 
"kernel trick" in order to implicitly map inputs into high-dimensional spaces in order to attempt to find a separating 
hyperplane. Predictions are given by whatever side of the gap the test data falls on.

This project also runs the same data sets through a support vector machine with default parameters, for comparison with 
the random forest classifier.

## Results

Random forests can be used to rank the importance of variables in a classification. During the fitting process an 
"out-of-bag" error is recorded for each data point, permuted among the training data and averaged to produce an 
"importance score" which is then normalised by the standard deviation of the error differences. Higher score values 
indicate high importance to the classifier. The relative feature importance for the preprocessed set is shown below.

![Relative feature importance](./images/relative_feature_importance.png)

Interestingly, derived feature "Title" is considered highly important. The low scores of "CabinLetter" and "CabinNo" 
are perhaps unsurprising considering most "Cabin" values were originally null.

Excluding values with relative importance of less than 20% – "CabinNo", "CabinLetter", "Port", "SibSp" and "Parch" – 
had little-to-no effect on the accuracy of the classifier and so are actually removed to reduce computation time.

The table below shows the comparison data between the simple data set and the preprocessed data set for random forests 
and SVM. The accuracy and f-measures are derived by cross-validating the training data against itself.
	
|                                           | Simple data                                            | Preprocessed data                                                  |
| ----------------------------------------- | :----------------------------------------------------- | :----------------------------------------------------------------- |
| **Prediction feature count**              | 6                                                      | 12                                                                 |
| **RF accuracy (no param. optimisation)**  | n/a                                                    | 0.82379                                                            |
| **RF f-measure (no param. optimisation)** | n/a                                                    | 0.76391                                                            |
| **RF accuracy**                           | 0.79461                                                | 0.83502                                                            |
| **RF f-measure**                          | 0.72808                                                | 0.77626                                                            |
| **RF ROC curve**                          | ![Simple data ROC curve](./images/simple_data_roc.png) | ![Preprocessed data ROC curve](./images/preprocessed_data_roc.png) |
| **SVM accuracy**                          | 0.83053                                                | 0.82043                                                            |
| **SVM f-measure**                         | 0.75917                                                | 0.75535                                                            |

As one would expect, the preprocessed data produces higher accuracy values for random forest classifiers than the 
simple data. However, the area under the receiver operating characteristic (ROC) curves for each interestingly returns 
similar results for both, with preprocessed data not statistically significantly better. It certainly does not suggest 
that one should have more confidence in the preprocessed data set. The SVM results are also comparable, with simple 
data perhaps performing slightly better.

## Discussion

The preprocessing steps for the random forest classifier successfully significantly increases the accuracy and 
f-measure through a combination of "instinctive" manual feature derivations and more formal methods like the 
random forest importance ranking.

Particularly interesting to see is that preprocessing with the aim of improving the random forest measure does not 
appear to improve the SVM. The slight drop in accuracy may be partially attributable to five features dropped after 
feature importance ranking.

It is also interesting to note derived features "Title" and "Age*Class" were considered important to the random forest 
classifier, along with those one might expect such as "Age" and "Gender". In the end, it appears that being a young, 
rich Countess would provide a good chance for survival.

## Future work

The next steps for this machine learning exploration are to spend more time analysing the confidence and statistical 
significance of the results and further investigating numerous classifiers. In particular, more work could be done on 
hyperparameter optimisation for SVMs as well as researching more deeply what "shape" of data produces better results 
for each classifier.

## Built with

* [Jupyter](https://jupyter.org/) - notebook development environment
* [MatPlotLib](https://matplotlib.org/) - used for results display
* [pandas](https://pandas.pydata.org/) - data preparation and manipulation
* [scikit-learn](http://scikit-learn.org/stable/) - machine learning with random forests and support vector machines

## Acknowledgements

* [Symbolon](https://thenounproject.com/symbolon/) - project icon
